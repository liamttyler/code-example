def tictactoe(): 
    index = ['#', '1', '2', '3', '4', '5', '6', '7', '8', '9']

    board = ['#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
    
    def display(board):
        print(board[1] + '|' + board[2] + '|' + board[3])
        print('-----')
        print(board[4] + '|' + board[5] + '|' + board[6])
        print('-----')
        print(board[7] + '|' + board[8] + '|' + board[9])
        
    def win_check(board):
        if ((board[1] == board[2] == board[3] != ' ')
            or (board[4] == board[5] == board[6] != ' ')
            or (board[7] == board[8] == board[9] != ' ')
            or (board[1] == board[4] == board[7] != ' ')
            or (board[2] == board[5] == board[8] != ' ')
            or (board[3] == board[6] == board[9] != ' ')
            or (board[1] == board[5] == board[9] != ' ')
            or (board[3] == board[5] == board[7] != ' ')):
            return True
        else:
            return False
    
    def draw_check(board):
        if (' ' not in board):
            return True
        else:
            return False
        
    print('index key')
    display(index)
    print('\n')
    
    game_on = True
    turn = 'X'
    accept_range = range(1,10) 
    
    print('Start of Game: ')
    display(board)
    
    while game_on:
        
        in_range = False
        
        choice = input(f"Player {turn} select move location (1-9): ")
        
        if choice.isdigit() == False:
            print ("Please enter number from 1-9")
        else:
            if int(choice) in accept_range:
                in_range = True
            else:
                print("Please enter number from 1-9")
        
        if in_range:
            if board[int(choice)] == ' ':
                board[int(choice)] = turn
            else:
                print("Please select an empty cell")
                continue
        
        display(board)
        
        if win_check(board):
            print('\n')
            print(f'Player {turn} is the winner!')
            game_on = False
        elif draw_check(board):
            print('Its a draw!')
            game_on = False
        else:
            if (turn == 'X'):
                turn = 'O'
            elif (turn == 'O'):
                turn = 'X'
            print('\n')
            print (f'Player {turn}s turn')